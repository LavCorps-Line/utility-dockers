let Prelude = ./Prelude.dhall

let Target = ./Target.dhall

let login =
      [ ''
        buildah login -u "$CI_DEPENDENCY_PROXY_USER" -p "$CI_DEPENDENCY_PROXY_PASSWORD" $CI_DEPENDENCY_PROXY_SERVER
        buildah login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
        ''
      ]

let prune = [ "buildah prune -af" ]

let _latestarch
    : forall (arch : Text) -> Text
    = \(arch : Text) ->
        ''
            buildah bud --tag "$CI_REGISTRY_IMAGE/$buildVer:latest" --manifest "$buildVer:latest" --arch ${arch} "$buildVer"
        ''

let _commitarch
    : forall (arch : Text) -> Text
    = \(arch : Text) ->
        ''
        buildah bud --tag "$CI_REGISTRY_IMAGE/$buildVer:commit-$CI_COMMIT_SHORT_SHA" --manifest "$buildVer:commit-$CI_COMMIT_SHORT_SHA" --arch ${arch} "$buildVer"
        ''

let _stablearch
    : forall (arch : Text) -> Text
    = \(arch : Text) ->
        ''
        buildah bud --tag "$CI_REGISTRY_IMAGE/$buildVer:stable" --manifest "$buildVer:stable" --arch ${arch} "$buildVer"
        ''

let _releasearch
    : forall (arch : Text) -> Text
    = \(arch : Text) ->
        ''
        buildah bud --tag "$CI_REGISTRY_IMAGE/$buildVer:release-$CI_COMMIT_TAG" --manifest "$buildVer:release-$CI_COMMIT_TAG" --arch ${arch} "$buildVer"
        ''

let latest
    : forall (target : Target) -> List Text
    = \(target : Target) ->
        let _latest =
              Prelude.Text.concat
                (   [ ''
                      if [ "$CI_COMMIT_BRANCH" = "$CI_DEFAULT_BRANCH" ]; then
                          buildah manifest create "$buildVer:latest"
                      ''
                    ]
                  # Prelude.List.map Text Text _latestarch target.platforms
                  # [ ''
                          buildah manifest push --all "$buildVer:latest" "docker://$CI_REGISTRY_IMAGE/$buildVer:latest"
                      fi
                      ''
                    ]
                )

        let _commit =
              Prelude.Text.concat
                (   [ ''
                      buildah manifest create "$buildVer:commit-$CI_COMMIT_SHORT_SHA"
                      ''
                    ]
                  # Prelude.List.map Text Text _commitarch target.platforms
                  # [ ''
                      buildah manifest push --all "$buildVer:commit-$CI_COMMIT_SHORT_SHA" "docker://$CI_REGISTRY_IMAGE/$buildVer:commit-$CI_COMMIT_SHORT_SHA"
                      ''
                    ]
                )

        in  [ Prelude.Text.concat (login # [ _latest ] # [ _commit ] # prune) ]

let stable
    : forall (target : Target) -> List Text
    = \(target : Target) ->
        let _stable =
              Prelude.Text.concat
                (   [ ''
                      buildah manifest create "$buildVer:stable"
                      ''
                    ]
                  # Prelude.List.map Text Text _stablearch target.platforms
                  # [ ''
                      buildah manifest push --all "$buildVer:stable" "docker://$CI_REGISTRY_IMAGE/$buildVer:stable"
                      ''
                    ]
                )

        let _release =
              Prelude.Text.concat
                (   [ ''
                      buildah manifest create "$buildVer:release-$CI_COMMIT_TAG"
                      ''
                    ]
                  # Prelude.List.map Text Text _releasearch target.platforms
                  # [ ''
                      buildah manifest push --all "$buildVer:release-$CI_COMMIT_TAG" "docker://$CI_REGISTRY_IMAGE/$buildVer:release-$CI_COMMIT_TAG"
                      ''
                    ]
                )

        in  [ Prelude.Text.concat (login # [ _stable ] # [ _release ] # prune) ]

let scheduled
    : forall (target : Target) -> List Text
    = \(target : Target) ->
        let _stable =
              Prelude.Text.concat
                (   [ ''
                      buildah manifest create "$buildVer:stable"
                      ''
                    ]
                  # Prelude.List.map Text Text _stablearch target.platforms
                  # [ ''
                      buildah manifest push --all "$buildVer:stable" "docker://$CI_REGISTRY_IMAGE/$buildVer:stable"
                      ''
                    ]
                )

        in  [ Prelude.Text.concat (login # [ _stable ] # prune) ]

in  { latest, stable, scheduled }
