let Target = ./Target.dhall

let Targets =
        [   { name = "alpine-sdk"
            , commands = [ "git --version" ] : List Text
            , stable = True
            , platforms = [ "amd64", "arm64" ] : List Text
            }
          : Target
        ,   { name = "buildah"
            , commands =
                  [ "buildah --version"
                  , "rm -rf /tmp/utility-dockers"
                  , "cp -a \$CI_PROJECT_DIR /tmp/utility-dockers"
                  , "buildah build -t \"registry.gitlab.com/lavcorps-line/infrastructure/utility-dockers/buildah:latest\" -f \"/tmp/utility-dockers/buildah/Dockerfile\" \"/tmp/utility-dockers/buildah\""
                  ]
                : List Text
            , stable = True
            , platforms = [ "amd64", "arm64" ] : List Text
            }
          : Target
        ,   { name = "dhall"
            , commands = [ "dhall version" ] : List Text
            , stable = True
            , platforms = [ "amd64" ] : List Text
            }
          : Target
        ,   { name = "haskell"
            , commands = [ "ghc -V", "stack --version" ] : List Text
            , stable = True
            , platforms = [ "amd64" ] : List Text
            }
          : Target
        ,   { name = "luajit"
            , commands = [ "luajit -v" ] : List Text
            , stable = True
            , platforms = [ "amd64", "arm64" ] : List Text
            }
          : Target
        ,   { name = "mdbook"
            , commands = [ "mdbook --version" ] : List Text
            , stable = True
            , platforms = [ "amd64", "arm64" ] : List Text
            }
          : Target
        ,   { name = "nushell"
            , commands = [ "nu -v" ] : List Text
            , stable = True
            , platforms = [ "amd64", "arm64" ] : List Text
            }
          : Target
        ,   { name = "pydev"
            , commands =
                  [ "git --version"
                  , "autoconf --version"
                  , "automake --version"
                  , "make --version"
                  , "gcc --version"
                  ]
                : List Text
            , stable = True
            , platforms = [ "amd64", "arm64" ] : List Text
            }
          : Target
        ,   { name = "rust"
            , commands = [ "rustup -V", "rustc -Vv", "cargo -V" ] : List Text
            , stable = True
            , platforms = [ "amd64", "arm64" ] : List Text
            }
          : Target
        ,   { name = "zsh"
            , commands = [ "zsh --version" ] : List Text
            , stable = True
            , platforms = [ "amd64", "arm64" ] : List Text
            }
          : Target
        ]
      : List Target

in  Targets
