# LavCorps-Line Utility Dockers

Starting with v3.20, container images support x86_64 and aarch64 architecture. 

---

To use the latest stable image, use:
* `registry.gitlab.com/lavcorps-line/infrastructure/utility-dockers/alpine-sdk:stable`
    * This image contains the latest stable release of Alpine Linux, built and
      configured to run Alpine Linux SDK tools (Building from APKBUILD)
* `registry.gitlab.com/lavcorps-line/infrastructure/utility-dockers/buildah:stable`
    * This image contains the latest stable release of Alpine Linux, built and
      configured to run Buildah (dockerfile building pipelines)
* `registry.gitlab.com/lavcorps-line/infrastructure/utility-dockers/dhall:stable`
    * This image contains the latest stable release of Alpine Linux, built and
      configured to run Dhall (programmable configuration language)
* `registry.gitlab.com/lavcorps-line/infrastructure/utility-dockers/ghcup:stable`
    * This image contains the latest stable release of Alpine Linux, built and
      configured to build Haskell packages.
* `registry.gitlab.com/lavcorps-line/infrastructure/utility-dockers/pydev:stable`
    * This image contains the latest stable release of Alpine Linux, built and
      configured to build Python packages.
* `registry.gitlab.com/lavcorps-line/infrastructure/utility-dockers/rustup:stable`
    * This image contains the latest stable release of Alpine Linux, built and
       configured to build Rust packages.

The latest stable image will be periodically regenerated for the latest updates.

---


To use the latest (unstable!) image, use:
* `registry.gitlab.com/lavcorps-line/infrastructure/utility-dockers/alpine-sdk:latest`
* `registry.gitlab.com/lavcorps-line/infrastructure/utility-dockers/buildah:latest`
* `registry.gitlab.com/lavcorps-line/infrastructure/utility-dockers/dhall:latest`
* `registry.gitlab.com/lavcorps-line/infrastructure/utility-dockers/ghcup:latest`
* `registry.gitlab.com/lavcorps-line/infrastructure/utility-dockers/pydev:latest`
* `registry.gitlab.com/lavcorps-line/infrastructure/utility-dockers/rustup:latest`

Unstable images are generated once, upon commit.
